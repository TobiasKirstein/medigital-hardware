#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include <string.h>
#include <HTTPClient.h>
#include <WiFi.h>
#include <ArduinoJson.h> //version 6


int scanTime = 5; //In seconds
BLEScan* pBLEScan;

int room1; 
int room2;
int nearest_room;
int bett = 33;

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {

      Serial.printf("Name: %s, "  ,advertisedDevice.getName().c_str());
      Serial.printf("Rssi: %d \n", (int)advertisedDevice.getRSSI());

      if (advertisedDevice.haveName()) {
      if (advertisedDevice.getName()== "Room1") {
        room1 = (int)advertisedDevice.getRSSI();} else{}
        }

      if (advertisedDevice.haveName()) {
      if (advertisedDevice.getName()== "Room2") {
        room2 = (int)advertisedDevice.getRSSI();} else{}
        }
      
         
    }
    
        

};

void setup() {
  Serial.begin(115200);

  WiFi.begin("Benjamin FrankLAN", "beliebigespasswort");   //WiFi connection 
 
  while (WiFi.status() != WL_CONNECTED) {  //Wait for the WiFI connection completion
 
    delay(5000);
    Serial.println("Waiting for connection");
 
  }

  
  Serial.println("Scanning...");

  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan(); //create new scan
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);  // less or equal setInterval value

  //setting Battery pinModes

pinMode(TX, OUTPUT);

//Setting Bed pinModes 
//Free
pinMode(14, OUTPUT);
//Clean
pinMode(12, OUTPUT);
//Ocupied
pinMode(26, OUTPUT);
//Broken
pinMode(2, OUTPUT);
}



void loop() {
  // put your main code here, to run repeatedly:
  BLEScanResults foundDevices = pBLEScan->start(scanTime, false);
  //Serial.print("Devices found: ");
  //Serial.println(foundDevices.getCount());
  //Serial.println("Scan done!");

  //eventuell hier noch nearest room auf none oder so setzen, falls keiner der beiden räume in reichweite ist

  if(room1<room2){
    nearest_room = 2;
  }

  if(room1>room2){
    nearest_room = 1;
  }

   if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
 
    HTTPClient http;    //Declare object of class HTTPClient

    char BedNumberString[10];
    sprintf(BedNumberString, "%d", bett);

    char RoomString[10];
    sprintf(RoomString, "%d", nearest_room);

    
    
    http.begin("http://medigital.herokuapp.com/bed_api_change_room");      //Specify request destination  --- je nachdem was wir machen wollen die entsprechende api hinterlegen. Aktuell (06.01.21) stehen bed_api_change_status, bed_api_change_room und bed_api_get_status zur auswahl
    http.addHeader("Content-Type", "application/json");                    //Specify content-type header

    //String json_message = "{\"BedNumber\": \"" BedNumberString"\",\"Room\":\""RoomString"\"}";

    char str[100];
    strcpy(str, "{\"BedNumber\": ");
    strcat(str, BedNumberString);
    strcat(str, ",\"Room\":");
    strcat(str, RoomString);
    strcat(str, "}");
    
    int httpCode = http.POST(str);                          //Send the request
    String payload = http.getString();                                    //Get the response payload
 
    Serial.println(httpCode);   //Print HTTP return code
    Serial.println(payload);    //Print request response payload

   //Battery status

   digitalWrite(TX, HIGH);

  
    
    
    //here goes the code that checks for status

    DynamicJsonDocument doc(2048);
    deserializeJson(doc, payload);
    
    // Read values
     String st = doc["Status"];
      Serial.println(st);

      if(st=="Available"){
        digitalWrite(14, HIGH);
        Serial.println("Available led an");
    } else {
        digitalWrite(14, LOW);
        Serial.println("available led aus");
    }

    if(st=="Occupied"){
        digitalWrite(26, HIGH);
        Serial.println("occ led an");
    } else {
        digitalWrite(26, LOW);
        Serial.println("occ led aus");
    }

    
    
    if(st=="Clean"){
        //digitalWrite(12, HIGH);
        Serial.println("clean led an");
    } else {
        //digitalWrite(12, LOW);
        Serial.println("clean led aus");
    }

    if(st=="Repair"){
        //digitalWrite(2, HIGH);
        Serial.println("Repair led an");
    } else {
        //digitalWrite(2, LOW);
        Serial.println("Repair led aus");
    }



    
 
    http.end();  //Close connection
 
  } else {
 
    Serial.println("Error in WiFi connection");
 
  }
  


  
      



  
  pBLEScan->clearResults();   // delete results fromBLEScan buffer to release memory
  delay(5500);
 
      
}
