#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h> //in der Ardino IDE ArduinoJson Version 5.13.5 installieren, nicht die neueste Version!


 
void setup() {
 
  Serial.begin(115200);                         //Serial connection
  WiFi.begin("Grooter", "der_ist_noch_gut");   //WiFi connection 
 
  while (WiFi.status() != WL_CONNECTED) {  //Wait for the WiFI connection completion
 
    delay(5000);
    Serial.println("Waiting for connection");
 
  }
 
}
 
void loop() {
 
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
 
    HTTPClient http;    //Declare object of class HTTPClient

    StaticJsonBuffer<300> JSONbuffer;                         //Declaring static JSON buffer -- das ist eine helfer klasse, die aus einzelnen variablen JSON macht, das die API lesen kann
    JsonObject& JSONencoder = JSONbuffer.createObject(); 
 
    JSONencoder["BedNumber"] = 33;    //so legt man die variablen fest, die an die Webapp übermittelt werden 
    JSONencoder["Room"] = 9999;
    
    char JSONmessageBuffer[300];
    JSONencoder.prettyPrintTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
    Serial.println(JSONmessageBuffer);
    
    http.begin("http://medigital.herokuapp.com/bed_api_change_room");      //Specify request destination  --- je nachdem was wir machen wollen die entsprechende api hinterlegen. Aktuell (06.01.21) stehen bed_api_change_status, bed_api_change_room und bed_api_get_status zur auswahl
    http.addHeader("Content-Type", "application/json");                    //Specify content-type header

    
    int httpCode = http.POST(JSONmessageBuffer);                          //Send the request
    String payload = http.getString();                                    //Get the response payload
 
    Serial.println(httpCode);   //Print HTTP return code
    Serial.println(payload);    //Print request response payload
 
    http.end();  //Close connection
 
  } else {
 
    Serial.println("Error in WiFi connection");
 
  }
 
  delay(30000);  //Send a request every 30 seconds  -- nur zum test. in echt natürlich nur bei raum änderung oder button klick
 
}
