#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h> //Jetzt brauchen wir version 6!

String st;

void setup() {

      pinMode(D7, OUTPUT);
      pinMode(D6, OUTPUT);

 
  Serial.begin(115200);                         //Serial connection
  WiFi.begin("Benjamin FrankLAN", "beliebigespasswort");   //WiFi connection 
 
  while (WiFi.status() != WL_CONNECTED) {  //Wait for the WiFI connection completion
 
    delay(5000);
    Serial.println("Waiting for connection");
 
  }
 
}
 
void loop() {

 
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
 
    HTTPClient http;    //Declare object of class HTTPClient
      http.useHTTP10(true);


 
    int BedNumber = 33;    //so legt man die variablen fest, die an die Webapp übermittelt werden 

    char BedNumberString[10];
    sprintf(BedNumberString, "%d", BedNumber);


    
    
    http.begin("http://medigital.herokuapp.com/bed_api_get_status");      //Specify request destination  --- je nachdem was wir machen wollen die entsprechende api hinterlegen. Aktuell (06.01.21) stehen bed_api_change_status, bed_api_change_room und bed_api_get_status zur auswahl
    http.addHeader("Content-Type", "application/json");                    //Specify content-type header

    //String json_message = "{\"BedNumber\": \"" BedNumberString"\"}";

    char str[100];
    strcpy(str, "{\"BedNumber\": ");
    strcat(str, BedNumberString);
    strcat(str, "}");

      Serial.begin(115200);  
      Serial.println(str);


    
    int httpCode = http.POST(str);                          //Send the request
    String payload = http.getString();                                    //Get the response payload
 
    Serial.println(httpCode);   //Print HTTP return code
    Serial.println(payload);    //Print request response payload

    DynamicJsonDocument doc(2048);
  deserializeJson(doc, http.getString());
  
  // Read values
     String st = doc["Status"];
  Serial.println(st);

      if(st=="Available"){
        digitalWrite(D7, HIGH);
        Serial.println("if");
    } else {
        digitalWrite(D7, LOW);
        Serial.println("else");
    }

    if(st=="Occupied"){
        digitalWrite(D6, HIGH);
        Serial.println("if");
    } else {
        digitalWrite(D6, LOW);
        Serial.println("else");
    }




 
    http.end();  //Close connection
 
  } else {
 
    Serial.println("Error in WiFi connection");
 
  }
 
  delay(10000);  //Send a request every 10 seconds  -- nur zum test. in echt natürlich nur bei raum änderung oder button klick
 
}
